<!--
SPDX-FileCopyrightText: 2024 Gregor Düster <git@gdstr.eu>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# TU Dresden CAmpus NAvigator ARchivierer

## Usage
Requirements:
* [python3-requests](https://packages.debian.org/sid/python3-requests) / [requests](https://pypi.org/project/requests/)
* [python3-pil](https://packages.debian.org/sid/python3-pil) / [pillow](https://pypi.org/project/pillow/)

```shell
# Display help
python3 download.py --help
python3 combine.py --help

# Downloads images, needs internet access,
# Results will be in ./downloaded/
python3 download.py APB00 APB01

# Combines images, doesn't need internet access but time,
# Results will be in ./combined/
python3 combine.py APB00 APB01
```
