# SPDX-FileCopyrightText: 2024 Gregor Düster <git@gdstr.eu>
#
# SPDX-License-Identifier: AGPL-3.0-only

from argparse import ArgumentParser
from multiprocessing import Pool
from pathlib import Path

from PIL import Image


def combine_all() -> None:
    with Pool() as pool:
        pool.map(combine, [path.name.replace("_8", "") for path in Path("downloaded").glob("*_8")])


def combine(level: str) -> None:
    im_filename: str = f"combined/{level}.png"

    # Skip already combined level
    if Path(im_filename).is_file():
        return

    paths = sorted(Path("downloaded").glob(f"{level}_8/*"))

    coordinates: list[list[str]] = [path.stem.split("_") for path in paths]
    size: tuple[int, int] = tuple((max(int(x[i]) for x in coordinates) + 1) * 480 for i in range(2))  # type: ignore[assignment]

    new_im = Image.new("RGBA", size, "WHITE")

    for path in paths:
        image = Image.open(path)
        pos = tuple(int(s) * 480 for s in path.stem.split("_"))
        new_im.paste(image, pos, image)  # type: ignore[arg-type]

    new_im.convert("RGB").save(im_filename)


def main() -> None:
    parser = ArgumentParser()
    parser.add_argument("levels", nargs="*")

    args = parser.parse_args()

    Path("combined").mkdir(exist_ok=True)

    if args.levels:
        for level in args.levels:
            combine(level)
    else:
        combine_all()


if __name__ == "__main__":
    main()
