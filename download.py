# SPDX-FileCopyrightText: 2024 Gregor Düster <git@gdstr.eu>
#
# SPDX-License-Identifier: AGPL-3.0-only

import json
import logging
import re
import urllib.parse
from argparse import ArgumentParser
from pathlib import Path
from typing import ValuesView

import requests

logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True


def download_all() -> None:
    session = requests.Session()

    html: str = session.get("https://navigator.tu-dresden.de/").text
    buildings_match: re.Match[str] | None = re.search(
        r" +this\.gebclick = ([^;]+);", html
    )

    if not buildings_match:
        logging.error("Could not find buildings, exiting.")
        return

    buildings = buildings_match.group(1)

    entry_levels: ValuesView = json.loads(buildings).values()

    # Buildings with level "--" don't have data available, also remove
    # duplicates
    entry_levels: set = set(filter(lambda i: not i.endswith("--"), entry_levels))  # type: ignore[no-redef]

    for entry_level in entry_levels:
        level_html: str = session.get(
            f"https://navigator.tu-dresden.de/etplan/{entry_level}"
        ).text
        levels: list = re.findall("href=[\"']etplan/([^\"']+)[\"']", level_html)

        for level in set(levels):
            level_url_part: str = level.replace("/", "").upper()
            download(level_url_part, session)


def download(level: str, session: requests.Session = requests.Session()) -> None:
    # Skip already downloaded level
    if Path(f"downloaded/{level}_1").is_dir():
        return

    # Determine size of maximum zoomed-out map, then guess (multiply by
    # eight) size of maximum zoomed-in version.  This is to prevent missing
    # out parts in the maximum zoomed-in map.
    pos: list[int] = [0, 0]
    for i in range(2):
        while True:
            response = session.get(
                f"https://navigator.tu-dresden.de/images/etplan_cache/{level}_1/{pos[0]}_{pos[1]}.png/nobase64"
            )
            if response.headers["Content-Length"] == "0":
                pos[i] -= 1
                break

            save_tile(response)

            pos[i] += 1

    if pos == [-1, -1]:
        print("ERROR: No content")

    pos = [(x + 1) * 8 for x in pos]

    for x in range(pos[0] + 1):
        for y in range(pos[1] + 1):
            response = session.get(
                f"https://navigator.tu-dresden.de/images/etplan_cache/{level}_8/{x}_{y}.png/nobase64"
            )
            if response.headers["Content-Length"] == "0":
                continue

            save_tile(response)


def save_tile(response: requests.Response) -> None:
    downloaded_dir = Path("downloaded")

    path: list[str] = urllib.parse.unquote(response.url).split("/")[5:7]
    (downloaded_dir / Path(path[0])).mkdir(parents=True, exist_ok=True)
    (downloaded_dir / Path("/".join(path))).write_bytes(response.content)


def main() -> None:
    parser = ArgumentParser()
    parser.add_argument("levels", nargs="*")

    args = parser.parse_args()

    if args.levels:
        for level in args.levels:
            download(level)
    else:
        download_all()


if __name__ == "__main__":
    main()
